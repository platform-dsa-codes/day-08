import java.util.*;
import java.io.*;

public class Main {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(br.readLine().trim());
        while (tc-- > 0) {
            String[] inputLine;
            int n = Integer.parseInt(br.readLine().trim());
            int[] arr = new int[n];
            inputLine = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                arr[i] = Integer.parseInt(inputLine[i]);
            }

            new Solution().rearrange(arr, n);
            for (int i = 0; i < n; i++) {
                System.out.print(arr[i] + " ");
            }
            System.out.println();
        }
    }
}

class Solution {
    void rearrange(int arr[], int n) {
        int[] positive = new int[n];
        int[] negative = new int[n];
        int posCount = 0, negCount = 0;

        for (int i = 0; i < n; i++) {
            if (arr[i] >= 0) {
                positive[posCount++] = arr[i];
            } else {
                negative[negCount++] = arr[i];
            }
        }

        int i = 0, posIndex = 0, negIndex = 0;

        while (posIndex < posCount && negIndex < negCount) {
            arr[i++] = positive[posIndex++];
            arr[i++] = negative[negIndex++];
        }

        // If positive numbers are remaining, add them to the end
        while (posIndex < posCount) {
            arr[i++] = positive[posIndex++];
        }

        // If negative numbers are remaining, add them to the end
        while (negIndex < negCount) {
            arr[i++] = negative[negIndex++];
        }
    }
}
