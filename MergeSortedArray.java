import java.util.Arrays;
import java.util.Scanner;

public class Solution {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = m - 1, j = n - 1, k = m + n - 1;

        while (i >= 0 && j >= 0) {
            if (nums1[i] > nums2[j]) {
                nums1[k] = nums1[i];
                i--;
            } else {
                nums1[k] = nums2[j];
                j--;
            }
            k--;
        }

        while (j >= 0) {
            nums1[k] = nums2[j];
            j--;
            k--;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the length of nums1 (m + n): ");
        int totalLength = scanner.nextInt();

        System.out.print("Enter the number of elements in nums1 (m): ");
        int m = scanner.nextInt();

        int[] nums1 = new int[totalLength];
        System.out.print("Enter the elements of nums1 separated by space: ");
        for (int i = 0; i < m; i++) {
            nums1[i] = scanner.nextInt();
        }

        System.out.print("Enter the number of elements in nums2 (n): ");
        int n = scanner.nextInt();

        int[] nums2 = new int[n];
        System.out.print("Enter the elements of nums2 separated by space: ");
        for (int i = 0; i < n; i++) {
            nums2[i] = scanner.nextInt();
        }

        Solution solution = new Solution();
        solution.merge(nums1, m, nums2, n);

        System.out.println("Merged nums1: " + Arrays.toString(nums1));
    }
}
