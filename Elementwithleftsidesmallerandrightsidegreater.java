import java.io.*;
import java.util.*;

class GFG {
	public static void main(String[] args) throws IOException
	{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine().trim());
        while(t-- > 0)
        {
            int n = Integer.parseInt(br.readLine().trim());
            int[] a = new int[n];
            String[] inputLine = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(inputLine[i]);
            }
            
            Compute obj = new Compute();
            System.out.println(obj.findElement(a, n));
            
        }
	}
}

class Compute {
    public int findElement(int arr[], int n){
        if (n < 3) {
            return -1;
        }

        int[] maxLeft = new int[n];
        int[] minRight = new int[n];

        // Fill maxLeft array
        maxLeft[0] = Integer.MIN_VALUE;
        for (int i = 1; i < n; i++) {
            maxLeft[i] = Math.max(maxLeft[i - 1], arr[i - 1]);
        }

        // Fill minRight array
        minRight[n - 1] = Integer.MAX_VALUE;
        for (int i = n - 2; i >= 0; i--) {
            minRight[i] = Math.min(minRight[i + 1], arr[i + 1]);
        }

        for (int i = 1; i < n - 1; i++) {
            if (arr[i] >= maxLeft[i] && arr[i] <= minRight[i]) {
                return arr[i];
            }
        }

        return -1;
    }
}
